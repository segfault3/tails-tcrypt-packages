-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gjs
Binary: gjs, gjs-tests, libgjs0e, libgjs-dev
Architecture: any
Version: 1.46.0-1.0tails2
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/Gjs
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gjs
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gjs
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, dbus-x11, gnome-desktop-testing, xauth, xvfb
Build-Depends: debhelper (>= 10), dpkg-dev (>= 1.17.14), gnome-common, gnome-pkg-tools, pkg-config, libglib2.0-dev (>= 2.50.3-2.0tails3), libgirepository1.0-dev (>= 1.50.0-1.0tails2), gobject-introspection (>= 1.50.0-1.0tails2), libmozjs-24-dev, libreadline-dev, libgtk-3-dev, libcairo2-dev, dbus <!nocheck>, dbus-x11 <!nocheck>, at-spi2-core <!nocheck>, xvfb <!nocheck>
Package-List:
 gjs deb interpreters optional arch=any
 gjs-tests deb interpreters optional arch=any
 libgjs-dev deb libdevel optional arch=any
 libgjs0e deb libs optional arch=any
Checksums-Sha1:
 7fcc71979563b5a587ab61f23f19ce258a9cf0eb 490812 gjs_1.46.0.orig.tar.xz
 404a838890f7013cffcf03b23eed894c355fd10e 9716 gjs_1.46.0-1.0tails2.debian.tar.xz
Checksums-Sha256:
 2283591fa70785443793e1d7db66071b36052d707075f229baeb468d8dd25ad4 490812 gjs_1.46.0.orig.tar.xz
 5527a0745e7ceec8811af49846ab29faeaea10a050bcf726dfb5cf098ec5b774 9716 gjs_1.46.0-1.0tails2.debian.tar.xz
Files:
 25574b60f4e1173fee0bd4920231df3e 490812 gjs_1.46.0.orig.tar.xz
 4172818bdc945ca218b0c4dc4c217a81 9716 gjs_1.46.0-1.0tails2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAltx3kIACgkQo7BMrpfw
+j5H8hAAgcXsbbIu+srXcRN3Lb+/xW22gzDf0qBnbRQnW6giKmwQedx3JBLZk/Ok
hAkEtaHJhNtqb5LkXqx6FyN2Y+c7Q7ngbtg2Un9dA+I/0CLrVHD3eu8ibh9oK8mr
haZ8sCZRTS+EAatUbOwEOjZgdOk3hNGeYkg+KJ+dAThRZWyz9nanpke1nKjLOjTO
TNk8IjFMOr+pJOExuSb5TQrs9rXE3aZz7pOLr/oR8bx8dwe7+uMXCw02H5BiymLk
2CbK22zK44JDe+Ea47R6dqT1veq1NFIWSBXJTz9gyz/IKJBCA7an/uLWqIcRCLHv
Q3D5+gK0LOZaUH4hUo/qG9VIDw21KK1fcOpH6qvHwqWRBYqXRsD3cs4kMP0YCziK
i5Jlij+8p/He/Rzi4rFjz5I7xy4YSJnzGSxhGhhrY7hkFdbo1kj427okk2XEB5fc
PLnSjKmzl2PC4+Rub/ya1dRAHBCej+fQo2I+ntFH2FWLZB9YHF6yfSFh7gKSSTBk
1ovYpu6KYBcr4YlAxkUNqfWBLCLILWo7gAeLj+ZIy3Qe2ILyYMaCUmggBictlEOX
i+ZAQDRyNv3I3tkgipkELAD8TSzVJjsk73ztewTWtaciVABClvOLTkqBxZLCAZXx
rpMqCwIdgrAtsNOVGmAl7ANceTqgoTX3SG8cwqsyfUzncjzKc0A=
=utWT
-----END PGP SIGNATURE-----
