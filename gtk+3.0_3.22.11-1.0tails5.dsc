-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gtk+3.0
Binary: libgtk-3-0, libgtk-3-0-udeb, libgtk-3-common, libgtk-3-bin, libgtk-3-dev, libgtk-3-doc, gtk-3-examples, gir1.2-gtk-3.0, gtk-update-icon-cache, libgail-3-0, libgail-3-dev, libgail-3-doc
Architecture: any all
Version: 3.22.11-1.0tails5
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>
Homepage: http://www.gtk.org/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gtk+3.0/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gtk+3.0/
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, python3-gi, xauth, xvfb
Build-Depends: debhelper (>= 9.20141010), cdbs (>= 0.4.93), gnome-pkg-tools (>= 0.11), dpkg-dev (>= 1.17.14), gtk-doc-tools (>= 1.20), dh-autoreconf, docbook-xml, docbook-xsl, pkg-config, autotools-dev, dbus <!nocheck>, gsettings-desktop-schemas <!nocheck>, adwaita-icon-theme <!nocheck>, at-spi2-core <!nocheck>, libglib2.0-dev (>= 2.50.3-2.0tails3), libgdk-pixbuf2.0-dev (>= 2.30.0), libpango1.0-dev (>= 1.37.3), libatk1.0-dev (>= 2.15.1), libatk-bridge2.0-dev, libegl1-mesa-dev [linux-any], libepoxy-dev, libfontconfig1-dev, libharfbuzz-dev (>= 0.9), libwayland-dev (>= 1.9.91) [linux-any], wayland-protocols (>= 1.7) [linux-any], libxkbcommon-dev (>= 0.2.0), libx11-dev, libxext-dev, libxi-dev, libxml2-utils, libxrandr-dev (>= 2:1.5.0), libxcursor-dev, libxcomposite-dev, libxdamage-dev, libxkbfile-dev, libxinerama-dev, libxfixes-dev, libcairo2-dev (>= 1.14.0), libcups2-dev (>= 1.2), libcolord-dev (>= 0.1.9), librest-dev, libjson-glib-dev, gobject-introspection (>= 1.41.3), libgirepository1.0-dev (>= 1.39.0), xauth <!nocheck>, xsltproc, xvfb <!nocheck>
Build-Depends-Indep: libglib2.0-doc, libatk1.0-doc, libpango1.0-doc, libcairo2-doc
Package-List:
 gir1.2-gtk-3.0 deb introspection optional arch=any
 gtk-3-examples deb x11 extra arch=any
 gtk-update-icon-cache deb misc optional arch=any
 libgail-3-0 deb libs optional arch=any
 libgail-3-dev deb libdevel optional arch=any
 libgail-3-doc deb doc optional arch=all
 libgtk-3-0 deb libs optional arch=any
 libgtk-3-0-udeb udeb debian-installer extra arch=any
 libgtk-3-bin deb misc optional arch=any
 libgtk-3-common deb misc optional arch=all
 libgtk-3-dev deb libdevel optional arch=any
 libgtk-3-doc deb doc optional arch=all
Checksums-Sha1:
 34204e745c137a31b93a74c72b7f6232d9b299ce 18250068 gtk+3.0_3.22.11.orig.tar.xz
 595b8eebdaa4728651448562a1c9bb6974c4b62c 148564 gtk+3.0_3.22.11-1.0tails5.debian.tar.xz
Checksums-Sha256:
 db440670cb6f3c098b076df3735fbc4e69359bd605385e87c90ee48344a804ca 18250068 gtk+3.0_3.22.11.orig.tar.xz
 de1d885da8889897b87587733553fc759be6ccf0bfe45aaa2a3242ae8afe544d 148564 gtk+3.0_3.22.11-1.0tails5.debian.tar.xz
Files:
 8fe8031c12c4f14d8ae7cd4256b0e3ce 18250068 gtk+3.0_3.22.11.orig.tar.xz
 984d0ff491d0aabd07317ef2b7f3320a 148564 gtk+3.0_3.22.11-1.0tails5.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAltx3kkACgkQo7BMrpfw
+j4QDA//a7NGhjqtYz1LoHKbw8kUlqNlPkWQUVtMk2nqpkPQkF3l7hZq1TKtBfAg
Qi3SlJ/jlcfHjYjAYNnbTiX7WQP/K/EA6xrGuQBtwZWHI2qPQFIiSTMr+gniZlJp
7E9nwAgDAwiWsMPvU4PRsIq4bPcB/ed6mHeD8jdRkst/X7w0np3K4JfWwbSHsQFt
NRb8OTPUQZdc68ae5UAnHlGFxmFM8zDGkiOPiTQblbcuMD0uaGAwNoaxRIj+VrgZ
jEIEdjXK3uzOxS8093CCjbkks9LNxF2OXsNgFaq7bYnyG832VQGb/MGmWjLHTM6x
g4a6zYchBmbhmQ86KSALishPJqoN6re26P4563NQiKe2Yr6NR4AeKMuEEtWEBPTr
ZtX6FS8p9Q5jFKhzTU5ijqwlsTlCtA+UBH0PoJLAWP4kJt4criaRoIa3rpfQ5A9X
Hj/xZYcJL8gaRuRMPbr+rIniwtoX274izJsVvO9anNmjcwOPWyxYbf3gJ1YdyjIu
CyAIq5QiuaTBHg2V0AYdQcNkPyeto6JR/RiF/itczXruT8Mv8L6AElWsvpfRrqso
ze6QRpy9WkbO/eRpSUsA7hzOwdGRJei49B3wVWd/uEDMkh83GB5LgRU0sardPksZ
LPBYwWvKIV1zd3CnRVyf2DdvO09vIaZvSmp1iPG9dnf8mAffqU0=
=IrZa
-----END PGP SIGNATURE-----
