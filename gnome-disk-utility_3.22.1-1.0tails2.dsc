-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gnome-disk-utility
Binary: gnome-disk-utility
Architecture: any
Version: 3.22.1-1.0tails2
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>, Martin Pitt <mpitt@debian.org>, Sjoerd Simons <sjoerd@debian.org>,
Homepage: http://git.gnome.org/cgit/gnome-disk-utility/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-utopia/gnome-disk-utility.git
Vcs-Git: git://anonscm.debian.org/pkg-utopia/gnome-disk-utility.git
Build-Depends: appstream-util, debhelper (>= 10), docbook-xsl, gnome-settings-daemon-dev (>= 3.8), intltool, libcanberra-gtk3-dev (>= 0.1), libdvdread-dev (>= 4.2.0), libglib2.0-dev (>= 2.31.0), libgtk-3-dev (>= 3.16.0), liblzma-dev (>= 5.0.5), libnotify-dev (>= 0.7), libpwquality-dev (>= 1.0.0), libsecret-1-dev (>= 0.7), libsystemd-dev (>= 209), libudisks2-dev (>= 2.1.8-1.0tails1), pkg-config, xsltproc
Package-List:
 gnome-disk-utility deb admin optional arch=any
Checksums-Sha1:
 ad9264be6990729b4d2ae3d5ba6378fa121a2dc1 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 fc37abaa714d9ff55d637e90e08614a2c8802925 11268 gnome-disk-utility_3.22.1-1.0tails2.debian.tar.xz
Checksums-Sha256:
 a3a4a187549f812e3837ae17dd9fa60afcacd84389d696058de254fe18b51ec3 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 0ddc750baf190a825f8647ae01dbf6bc9863fa47a71c46ce93bb67b7dcd09294 11268 gnome-disk-utility_3.22.1-1.0tails2.debian.tar.xz
Files:
 ecb9ace36964c279f8093f5e34cf3799 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 193ffa670773e4b356fcbcc25379ed78 11268 gnome-disk-utility_3.22.1-1.0tails2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAlu6a0QACgkQo7BMrpfw
+j4emQ//QJf5ODWWP31ZWNvZmIWZdLiJI61y0sLKWlPnHyLEsAwa+Dec6aw62cT/
q3o0BY3BiNiUyoExuqfwammAnriTMDpkgDklDL/EHUKR4x6QUxXxB7cleN8YJFDV
ackfY5QZxnpRSBne2L1Yq0eFAbDA9DDLqbqPUBI5hhT+FNr6BupF6TNqt9XpHqKS
b+ZcTGCPJO8WBV1BwK/+mIQ3dPcRklIbmA2Pu/JHFEdbR9Rt/W9YuiCG8gkXOaLt
GrP9lqYnDST7pKclCnthlPpTEdErdIJ2OtbIfza60vDatoC1ID6U2ozWoL6Kzijf
Sm9iEJ6BzBbs3rucrDNzZ9M10PrrCe84xYwJUiyH36LDCTqMkmU383j+k3Y1Skb3
oaZlFI6Bu3ccGyW1giXNA1ipRNucw5eTd0DJHbZLIhUxfvSboAv59gSi+U0MmTV0
9nsZiDZ5dwRfLLV9TbDX4VKJYp+5MvSLrwbzCltekb0EI4TUDI3r0+Vf/ZeSzVDp
o+bwsw0nmliEsIO2rAMclFuhG43adu5z1qoOD/Os8ZnPfy+Ji1bnzz+IRxztsYHj
yFwRWfvNe08sYEUb747lYH3w1T5ezeDsTAvj6HSYkz/c4jYFS+PqaUXn6A9slwmi
T+BHgmiPnN5TtAI7Gp3LcS4AJssZkpK0+HQ7QdO8Z3+Theg2MfU=
=P+PL
-----END PGP SIGNATURE-----
