-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gobject-introspection
Binary: libgirepository-1.0-1, libgirepository1.0-dev, libgirepository1.0-doc, gobject-introspection, gir1.2-glib-2.0, gir1.2-freedesktop
Architecture: any all
Version: 1.50.0-1.0tails2
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Emilio Pozuelo Monfort <pochu@debian.org>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/GObjectIntrospection
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gobject-introspection/
Vcs-Svn: svn://anonscm.debian.org/svn/pkg-gnome/desktop/unstable/gobject-introspection
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, file, gir1.2-gtk-3.0, libcairo2-dev
Build-Depends: debhelper (>= 10), gnome-pkg-tools (>= 0.10), python3-dev, pkg-config, flex, gtk-doc-tools (>= 1.19), bison, libglib2.0-dev (>= 2.50.3-2.0tails3), libcairo2-dev, libffi-dev (>= 3.0.0), libtool, python3-mako
Build-Depends-Indep: libglib2.0-doc
Package-List:
 gir1.2-freedesktop deb introspection optional arch=any
 gir1.2-glib-2.0 deb introspection optional arch=any
 gobject-introspection deb devel optional arch=any
 libgirepository-1.0-1 deb libs optional arch=any
 libgirepository1.0-dev deb libdevel optional arch=any
 libgirepository1.0-doc deb doc optional arch=all
Checksums-Sha1:
 5907f70bd275dceb4d3cb825ddce82cb1fdb4242 1415700 gobject-introspection_1.50.0.orig.tar.xz
 0f3a131834835af09ee7401a03a4f2ab38a90263 19236 gobject-introspection_1.50.0-1.0tails2.debian.tar.xz
Checksums-Sha256:
 1c6597c666f543c70ef3d7c893ab052968afae620efdc080c36657f4226337c5 1415700 gobject-introspection_1.50.0.orig.tar.xz
 353029c0c3594f679be3aedf4e0645165300c77702a816146333a19ab4f70936 19236 gobject-introspection_1.50.0-1.0tails2.debian.tar.xz
Files:
 5af8d724f25d0c9cfbe6df41b77e5dc0 1415700 gobject-introspection_1.50.0.orig.tar.xz
 a42935d26d62e9ef781bae59a35bb95e 19236 gobject-introspection_1.50.0-1.0tails2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAltx3kkACgkQo7BMrpfw
+j5azA/9HFoBK2muu01gsgMZZjuhB7TEYeJ+CUFe6UWjnRcyP6wu4B8BXZv+K0KN
mGzzIxf4RNC1viI49rPrbvSx6EEDkMdOOZl+VMKgUcQYzf20Hf4zPv+h6MLrzuup
W9TxL+aqPhHdRB9Py/CN6eSleuKAJaDuTDgae2EN/xVBfRMFcARFmq1FTNQUSAd7
YRhlY5vEkPW9PznVLxIvRo4dddEc8U9ltVdF/fCItCcTbmzxp1ycp1QYsU1jyO4L
HtzzWh6fvfi/mjfH5S9A6mV2F21DCF7TU+1lWYpcOSgliIcEC1YGkRNqsQlJ3Tia
kDGB5bWSqM3QM1tGUbAAFNhekYcPt4w5r3GLLOqAji+an0XEjo4T5LgoqDkqF8/S
oJwjVTiRCpswtFwnJhigQZMgGWKclEzQ5yEoZV+fNhWbSmWJTKQOZORdphrwld71
RFkAhoeuMx9iaOxVb2NSZNnhMy/uMYdM6wWFptfWWSz8j9VIzzBmVcSagP6Ir/VY
/9m+jda48thxJ8DdrEJHwwSnyNkSHBBNRARXw5GSt6+hf1cDQAjfiitwjhy1ky57
OnPngfPkDPsjnwEUVoA1zp9qdkuUB+sgcnqQqlZP+8u8lSCDL112gMnFbVTVLZMZ
aIEQX58pDZxWMtiMZEihDauDVRjCh9hJyA+yy4MsxE/+tkq/cTA=
=8eIO
-----END PGP SIGNATURE-----
