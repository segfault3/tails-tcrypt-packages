-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: udisks2
Binary: udisks2, udisks2-doc, libudisks2-0, libudisks2-dev, gir1.2-udisks-2.0
Architecture: linux-any all
Version: 2.1.8-1.0tails4
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>, Martin Pitt <mpitt@debian.org>,
Homepage: https://www.freedesktop.org/wiki/Software/udisks
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-utopia/udisks2.git
Vcs-Git: https://anonscm.debian.org/git/pkg-utopia/udisks2.git
Testsuite: autopkgtest
Testsuite-Triggers: cryptsetup-bin, dosfstools, gir1.2-glib-2.0, kpartx, libatasmart-bin, lvm2, make, mdadm, policykit-1, python3-dbus, python3-gi, reiserfsprogs, xfsprogs
Build-Depends: debhelper (>= 9), dh-autoreconf, gobject-introspection (>= 1.30), gtk-doc-tools, intltool (>= 0.40.0), gnome-common, libacl1-dev, libatasmart-dev (>= 0.17), libgirepository1.0-dev (>= 1.30), libglib2.0-dev (>= 2.31.13), libgudev-1.0-dev (>= 165), libpolkit-agent-1-dev (>= 0.97), libpolkit-gobject-1-dev (>= 0.97), libsystemd-dev (>= 209), pkg-config, udev (>= 147), xsltproc
Package-List:
 gir1.2-udisks-2.0 deb introspection optional arch=linux-any
 libudisks2-0 deb libs optional arch=linux-any
 libudisks2-dev deb libdevel optional arch=linux-any
 udisks2 deb admin optional arch=linux-any
 udisks2-doc deb doc optional arch=all
Checksums-Sha1:
 8e38580b435986570bb7b784facf387757d2de43 931110 udisks2_2.1.8.orig.tar.bz2
 cddd8ca63fb2df570baf6a6fc83ac51e08088e03 19052 udisks2_2.1.8-1.0tails4.debian.tar.xz
Checksums-Sha256:
 da416914812a77e5f4d82b81deb8c25799fd3228d27d52f7bf89a501b1857dda 931110 udisks2_2.1.8.orig.tar.bz2
 b0979789d9981fdc826236439d5b4b5af37776be7b3f1b07d31d47bfe24a6c8f 19052 udisks2_2.1.8-1.0tails4.debian.tar.xz
Files:
 501d11c243bd8c6c00650474cd2afaab 931110 udisks2_2.1.8.orig.tar.bz2
 5ecbffc0bdddb5ffa52d580666635e85 19052 udisks2_2.1.8-1.0tails4.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAlurbBYACgkQo7BMrpfw
+j7CaQ/8CyBa8I5DoFsKRcYar0wznx3U1JySw5iTcHaQu4i5v/POeeswUI0dhAO3
c+2l6AVPxnvxiJGJQFC6AF5i4dgKtZ51Ru/hRCzIZvxEi77okW/EWAHSDvqrfK+6
8/aEd0FWU+cb2O2mUo/CZfIo4Yx9yHhetX1Nk3a8zFPU+eZJTU9Q59mGIVsbdxeD
bxfoRE7EveMJVqwPeSZGHkPBmP0v8z2C5ya9MhAxSsfvyQCRIOl9aXa7dxQVvSz5
neZtkSQa3gA+AjJ/QlH9lX3XG17PAtuchIzmrksTCx9jHKjVpv7qGviYOiv4kk/i
IldKbtyJc94Wg4ya+WIFPDprt6UWgTUjo6rNSK9GyqG3fy9yzX39L3GfkyKT5Z8e
zLMO4vCiLLy0qJCpfcxQlr+X/zBCRX018rwJsR1JZc2E7XrAGfqh5cPG4+CtMRq9
ekrqzUq9vI9nYafn+ZZSsfcyKgKaGrbHd2GSU1NgLO6TIil1A61TcuCmjIu5q4g4
UklFQ6s43/kIu8VoQqFHweom0ajhJaCCMZlI8U+BPib/TSfZVRshm9AveOx3nK6n
8W9JGV8HCU83sQMPb4nENSqtuuiKa5uHdKzxJ7zgNCLdet99iyszyfetOUMcr92J
rUrAmWxWRyi82YOB3TEC6uqCNf8EiNfcIxTOH9yyWdKxioiSvLo=
=yyfm
-----END PGP SIGNATURE-----
