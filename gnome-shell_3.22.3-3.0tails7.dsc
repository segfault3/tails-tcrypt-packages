-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gnome-shell
Binary: gnome-shell, gnome-shell-common
Architecture: linux-any all
Version: 3.22.3-3.0tails7
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Emilio Pozuelo Monfort <pochu@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/GnomeShell
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gnome-shell/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gnome-shell
Build-Depends: debhelper (>= 10), gnome-control-center-dev (>= 1:3.4), gnome-pkg-tools (>= 0.11), gettext, pkg-config (>= 0.22), autoconf-archive, gobject-introspection (>= 1.49.1), gsettings-desktop-schemas-dev (>= 3.21.3), gtk-doc-tools, libatk-bridge2.0-dev, libcanberra-dev, libcanberra-gtk3-dev, libcroco3-dev (>= 0.6.8), libecal1.2-dev (>= 3.7.90), libedataserver1.2-dev (>= 3.17.2), libgcr-3-dev (>= 3.7.5), libgirepository1.0-dev (>= 1.50.0-1.0tails2), libgjs-dev (>= 1.46.0-1.0tails2), libglib2.0-dev (>= 2.50.3-2.0tails3), libgnome-bluetooth-dev (>= 3.9.0) [linux-any], libgnome-desktop-3-dev (>= 3.7.90), libgnome-menu-3-dev, libgstreamer1.0-dev (>= 0.11.92), libgtk-3-dev (>= 3.21.6), libibus-1.0-dev, libmutter-dev (>= 3.22.1), libnm-glib-dev (>= 0.9.8) [linux-any], libnm-glib-vpn-dev (>= 0.9.8) [linux-any], libnm-gtk-dev (>= 0.9.8) [linux-any], libnm-util-dev (>= 0.9.8) [linux-any], libpolkit-agent-1-dev (>= 0.100), libpulse-dev (>= 2.0), libsecret-1-dev, libstartup-notification0-dev (>= 0.11), libsystemd-dev [linux-any], libtelepathy-glib-dev (>= 0.17.5), libx11-dev, libxfixes-dev, libxml2-dev, mesa-common-dev, python3
Package-List:
 gnome-shell deb gnome optional arch=linux-any
 gnome-shell-common deb gnome optional arch=all
Checksums-Sha1:
 0cfb5d638fb5b9ea02421fdab21c1ebfd145de06 1952576 gnome-shell_3.22.3.orig.tar.xz
 7baf2203b0bebade2dceb8f60414f2ed45d318e9 168328 gnome-shell_3.22.3-3.0tails7.debian.tar.xz
Checksums-Sha256:
 d1e6bd80ddd1fef92d80b518d4dbeffa296e8f003402551b8c37c42744b7d42f 1952576 gnome-shell_3.22.3.orig.tar.xz
 2b40437d0e682df9ddc8b57caa786c4e0d0e18b572ba1020f10fddfce365f9e7 168328 gnome-shell_3.22.3-3.0tails7.debian.tar.xz
Files:
 cb2d9ec7f80a72808d89f5db5560ece8 1952576 gnome-shell_3.22.3.orig.tar.xz
 ff6a3ae1f4423ddcbfb812685038be6f 168328 gnome-shell_3.22.3-3.0tails7.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAlu6eF8ACgkQo7BMrpfw
+j4u0w//S0lpcaGYhZYvQq4WIDN4UwOpijnh27EjWCQRh0UlJI/hlJTKYtWx/q5i
j3iy1V8d9BafErkqDbaxaI34ttAm+PKYs01ChaDTK+fGN2iDdKL9Aoy8gP2yaxPO
VAw8SkImBr3jrNI6uGogDQDP4nUYKNCH3ldDMP/KKDR5lRge5u2w3XCDObqCTKi1
G6yApgcMOrmD1ILh2M//1p/KGS8lzNUENd5hXqJE8tATWA8XRaHJUa0ogmAabs4D
wzGMjE28S9Az9c8Ag7ut1Jy34Xs9UetueLzOwnvSdDrQmk4m27PBkf1CK6abD7gu
vujO17WOl6yrhIB+GX/COXzqYysTGwZIFocSRMz+XIGG2c443nxl+qW6RbY2cdeD
D/osFV6VI4Zux9mtShyBwiZQYiOfkVz+4565Pr6ras9YT36JpzXG+tLRrJutCll7
WInijYE2RWv6KqiZg/aZIJU5ueewjbl3Dp//NYalgXJbMfEMTRZsC4mgfWeBt/uS
9BEV9AU3lAQpChea/vTTXNoqCVi8vCHbRwGUjOTrtngffD9O2NSczcyfRGm8KL5C
KERA7xMT+FgmnKO7TqOTnDK0xTUShNsSTDh8SvjsMergSUVdmw5xUjTbcQSyIUwu
We4Xv0m+vbNWxWdXqrBW2BmNIt56J8BOYdISsBnS8Uwem27hKN8=
=TWYm
-----END PGP SIGNATURE-----
