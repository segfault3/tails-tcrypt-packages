-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: glib2.0
Binary: libglib2.0-0, libglib2.0-tests, libglib2.0-udeb, libglib2.0-bin, libglib2.0-dev, libglib2.0-0-dbg, libglib2.0-data, libglib2.0-doc, libgio-fam
Architecture: any all
Version: 2.50.3-2.0tails3
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: http://www.gtk.org/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/glib2.0/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/glib2.0/
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, dbus-x11, gnome-desktop-testing, xauth, xvfb
Build-Depends: debhelper (>= 9.20141010), cdbs (>= 0.4.93), dh-autoreconf, dh-exec, pkg-config (>= 0.16.0), gettext, autotools-dev, gnome-pkg-tools (>= 0.11), dpkg-dev (>= 1.17.14), libelf-dev (>= 0.142), libmount-dev (>= 2.28) [linux-any], libpcre3-dev (>= 1:8.35), gtk-doc-tools (>= 1.20), libselinux1-dev [linux-any], linux-libc-dev [linux-any], libgamin-dev [!linux-any] | libfam-dev [!linux-any], zlib1g-dev, desktop-file-utils <!nocheck>, dbus <!nocheck>, shared-mime-info <!nocheck>, tzdata <!nocheck>, xterm <!nocheck>, python3:any (>= 2.7.5-5~), python3-dbus <!nocheck>, python3-gi <!nocheck>, libxml2-utils, xsltproc, docbook-xml, docbook-xsl, libffi-dev (>= 3.0.0)
Package-List:
 libgio-fam deb libs optional arch=hurd-any,kfreebsd-any
 libglib2.0-0 deb libs optional arch=any
 libglib2.0-0-dbg deb debug extra arch=any
 libglib2.0-bin deb misc optional arch=any
 libglib2.0-data deb libs optional arch=all
 libglib2.0-dev deb libdevel optional arch=any
 libglib2.0-doc deb doc optional arch=all
 libglib2.0-tests deb libs optional arch=any
 libglib2.0-udeb udeb debian-installer optional arch=any
Checksums-Sha1:
 3177471fb11fd2fd5abb77ec0675ee0049521009 7589284 glib2.0_2.50.3.orig.tar.xz
 749dec28816d17e0a41d7b5ab065ebbaac1c06dd 74516 glib2.0_2.50.3-2.0tails3.debian.tar.xz
Checksums-Sha256:
 82ee94bf4c01459b6b00cb9db0545c2237921e3060c0b74cff13fbc020cfd999 7589284 glib2.0_2.50.3.orig.tar.xz
 b61d35a2185e986d1ba4466cae2efe4b08fb834e86b003c6eb8eaff32531b8ee 74516 glib2.0_2.50.3-2.0tails3.debian.tar.xz
Files:
 381ab22934f296750d036aa55a397ded 7589284 glib2.0_2.50.3.orig.tar.xz
 5f2e4252a67629bda0c5ced6e2c31acc 74516 glib2.0_2.50.3-2.0tails3.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAlty9ZsACgkQo7BMrpfw
+j7VkA//eaXJLUkRzIUql9L/0cbof7w0cvW25LT7gaQmMmrC94nL4/Eb7ZIIgIJi
3KpeLTf/sBQkQRYkh68TR7hAW3iTai6+LlGBUhVvQINNfsUkQ8vaGofNZ3jujBCs
EusKx9I+IgJ89hfsYrom1ravCzsuni5VIeWt90V/NyX5SFQ/vsRZ8piKZJviaN5y
wjp2K38LN9nE/aDXeriqxSsBR94jsINwaF/t7kPSvpKPyNJnaPFBZ+vci0nPq+W8
+BB4595Tn8ZvYYtbAMIs6hg8izpUK7xN9XmautWJYVN/nDmoNxnvu1ZbE8UQqEk1
qQ2DCtMkE+zPXoZ+UmQuLzGTy3NySVcV2nxYNKzeDAfmEZWNLzdRpEPVrQetfFc8
hxMJZCalbFSGone5aa4mMve46VM8DiX/SYruQdM19MEsvTbUWWjaSmK2vxgKgZHx
yjDDi9upMg5YxfgoLKzcz5p341Lm+sMNMQ+3Oas3Gauu2Mgzs70+u75yEIWFrpOy
Nwj1LqMOTEU4js8nDlQHqObTBuFR9UXe/UQUc2+IuKpZJOcy8AIljvpnV11X/PsN
YvIzBRvHItGaLccDc9Hwh7Fcf1AT+9HjJvDpvyUxnpbmREveA1TJ8gRhsStm43yL
R5ZLnI0g3CuEDtI20+N9zhqiMGiRox87bEbud1kTXAwEC/aVAxA=
=4vcJ
-----END PGP SIGNATURE-----
