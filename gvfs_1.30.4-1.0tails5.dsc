-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gvfs
Binary: gvfs, gvfs-daemons, gvfs-libs, gvfs-common, gvfs-fuse, gvfs-backends, gvfs-bin
Architecture: any all
Version: 1.30.4-1.0tails5
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Laurent Bigonville <bigon@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/gvfs
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gvfs/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gvfs
Testsuite: autopkgtest
Testsuite-Triggers: apache2, apache2-dev, dbus-x11, gir1.2-umockdev-1.0, python-twisted-core, python3-gi, samba, umockdev
Build-Depends: debhelper (>= 10), dh-exec (>= 0.13), gnome-pkg-tools (>= 0.7), pkg-config, gtk-doc-tools, libcap-dev [linux-any], libglib2.0-dev (>= 2.50.3-2.0tails3), libdbus-1-dev, openssh-client, libsoup2.4-dev (>= 2.42.0), libxml2-dev, libudev-dev (>= 0.138) [linux-any], libavahi-glib-dev (>= 0.6), libavahi-client-dev (>= 0.6), libfuse-dev [!hurd-any], libgudev-1.0-dev (>= 147) [linux-any], libcdio-paranoia-dev (>= 0.78.2), libbluetooth-dev (>= 4.0) [linux-any], libgoa-1.0-dev (>= 3.17.1), libgdata-dev (>= 0.17.3), libexpat1-dev, libgphoto2-dev (>= 2.5.0), libsecret-1-dev, libbluray-dev, libmtp-dev (>= 1.1.6), libpolkit-gobject-1-dev, libsmbclient-dev (>= 3.4.0) [!hurd-any], libarchive-dev, libgcrypt20-dev (>= 1.2.2), libltdl-dev, libimobiledevice-dev (>= 1.2) [!hurd-any], libplist-dev, libudisks2-dev (>= 2.1.8-1.0tails1) [linux-any], libsystemd-dev [linux-any], systemd (>= 206) [linux-any], libgcr-3-dev, libgtk-3-dev, libnfs-dev (>= 1.9.7)
Package-List:
 gvfs deb libs optional arch=any
 gvfs-backends deb gnome optional arch=any
 gvfs-bin deb gnome optional arch=any
 gvfs-common deb libs optional arch=all
 gvfs-daemons deb libs optional arch=any
 gvfs-fuse deb gnome optional arch=linux-any,kfreebsd-any
 gvfs-libs deb libs optional arch=any
Checksums-Sha1:
 77a6f86b9fb83bcee308592e90af4f13983e794b 1895304 gvfs_1.30.4.orig.tar.xz
 88edf73ec0f6d51487033edcd27028084deebcfa 23068 gvfs_1.30.4-1.0tails5.debian.tar.xz
Checksums-Sha256:
 981e0aca7f4e2e99860137f9fd99c335fa72a764156d253caf1069380a8e3afa 1895304 gvfs_1.30.4.orig.tar.xz
 27cdc2c6c5ea1eecdb8ab4d9df8a97f525cc85b1fe22c2a3686959073e8e30cf 23068 gvfs_1.30.4-1.0tails5.debian.tar.xz
Files:
 4774661e1a4982a0476ba0f30c52050c 1895304 gvfs_1.30.4.orig.tar.xz
 b4d53afad4c5f274c540c8796c2d5dc7 23068 gvfs_1.30.4-1.0tails5.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEzeyHuMBqtRyaJ5k9o7BMrpfw+j4FAltx3koACgkQo7BMrpfw
+j7Fwg//afJSKa3p93vt/Ma87Fv8wAXGciTnK07VqMiRVDkZ4lCqxaeP8kYhRRrZ
JC17PMrcs7rWt9cCmEJVI3mtFrtCpMNL+g0JojuGtTFgz6LCj/OpbNyE7hW7pTTB
cr946LIuunkh6Wu9tc7h/PpGOPAnjF8pmjcJh85B6KAW1gEwUWvwZDaNeT/z02rD
QrS7Jk1QR7srPiP2gxnb0h5Bv/dL5JRIJ4GU7+BJJLuFHWy/4o1+E8Z0NKGARahF
qFOBtn5sNd8LpeNhT2el4bFsJG0g+A15FKA8LsA/aJgCZ6RPdWg6kd1b2ozjAGqT
xhuBVRdY6tVvEIwb0j8WH52n+OsyDhaJaPPTz2NkewU/SCrXsO5a/3cYR8udN8c2
yujnmwXL96TQZ5f14MN5vzr+ds7KGbs5oLXvvE5q4GyRfRIa5h0I3tTzB7alsi/k
1FgVyTd2MYBi4IVYnk4c9vQbVZePcg6zSjztLI/sJsw1Sz6Vu33zf36WA1AhgL5I
kBQxT41aSaNT8NwNExBShG+FmHTENnqLQ0KMsObjXUh45DnMEXjiOwP2Yc4uvRnB
xgntHt7ayLX5kIzrxrlaHfxlyDB1ol4GJ082Da0tSHQ9V0lkiicAiOTcYBox2fOA
zuLmb4f57J5r6Gr9z0L5XrDnc4kVlG82BZ0g/rCKEbpxb0a8vRg=
=C2oK
-----END PGP SIGNATURE-----
